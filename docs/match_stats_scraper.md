# Match Stats Scraper Script

This Python script is designed to scrape match statistics, including penalties, goals, cards, substitutions, and more
from the OL website. It allows you to specify a range of match IDs and a season to retrieve data for.

## Prerequisites

There are no special prerequisites required to run this script.

## Usage

### Command Line Arguments

`-b --begin`: The start match ID.
`-t --to`: The end match ID.
`-s --season`: The season for which you want to scrape match statistics.
`-l --location`: (Optional) Specify the location (DE/AT/CH/UK). If not provided, the script will prompt you to select a
location.
- `-o --output`: Path to the output directory. (Default: "output")

### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/match_stats_scraper.py -b <begin> -t <to> -s <season> -l <location>
```

Replace `<begin>` and `<to>` with the match ID range you want to scrape, `<season>` with the season for which you want
to
scrape match statistics, and `<location>` with the Online Liga location (e.g., onlineliga.de).

### Example Usage

```bash
python onlineliga_script_collection/match_stats_scraper.py -b 1001 -t 1010 -s 31 -l onlineliga.de
```

This command will scrape match statistics for matches with IDs from 1001 to 1010 in the 31 season on the "
onlineliga.de" website.

### Output

This script will scrape match statistics for the specified range of match IDs and season. The CSV file will contain the
following match statistics:

- match_id
- season
- missed_pens (Missed Penalties)
- successful_pens (Successful Penalties)
- goals
- own_goals
- system_changes (System Changes)
- yellow_cards
- yellow_red_cards
- red_cards
- substitutions
- injuries
- status (Match Status)
- url (Match URL)

The CSV file will be named based on the location and timestamp of the script execution,
e.g., `match_stats_onlineliga.de_2023-09-08_15-30-45.csv`.
