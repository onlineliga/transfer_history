# Transfer History Team Scraper Script

This Python script is designed to scrape team transfer history data from the OL website. It allows you to
specify a list of user IDs, a season, and the OL location, and then retrieve team transfer history details for each
user. The data is saved in an Excel (XLSX) file for further analysis.

## Prerequisites

There are no special prerequisites for this script.

## Usage

### Input Configuration

To use the script, you need to create an input configuration file in JSON format. The configuration file should include
the following parameters:

- `season`: The OL season for which you want to scrape team transfer history.
- `user_ids`: A list of user IDs for which you want to retrieve transfer history data.
- `location`: The Online Liga website location (DE/AT/CH/UK). This determines the website to scrape.
- `-o --output`: Path to the output directory. (Default: "output")

Here's an example input configuration:

```json
{
    "season": 9,
    "user_ids": [
        111,
        2597,
        138
    ],
    "location": "onlineleague.co.uk"
}
```

**_Important Note:_** This configuration file needs to be saved in the `configuration_files` directory and named `team_transfers_input.json`.

### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/transferhistory_team.py
```

### Output
The script will scrape team transfer history data for the specified user IDs and season. The data will be saved in an
Excel (XLSX) file with a filename based on the location, season, and timestamp of the script execution, e.g.,
`th_team_onlineleague.co.uk_season9_2023-09-08_15-30-45.xlsx`. The Excel file will contain the following columns:

- VereinsID (User ID)
- Spielername (Player Name)
- andererVerein (Other Team)
- playerid (Player ID)
- betrag (Transfer Fee)
- position (Player Position)
- Alter (Player Age)
- zeitpunkt (Transfer Date)
- Saison (Season)
- Schlüssel (Key)
