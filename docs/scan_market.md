# Scan Market Script

The Scan Market script allows you to scan the player transfer market in OL for specific player offers. This script is
designed to be used with given OL servers and provides a convenient way to filter and retrieve player transfer offers
based on custom criteria.

## Features

- Customizable player attribute filters.
- Retrieves player information, including their name, positions, and additional offer details.
- Saves the filtered player offers to an Excel file for analysis.

## Prerequisites

This script requires login credentials for the OL website. The credentials should be stored in a file
called `credentials.json` in the `configuration_files` directory. Please refer to
the [Credentials](../README.md#credentials) section in the main README for more information.

## Usage

### Command Line Arguments

The script accepts the following command line arguments:

- `-l, --location`: The location of the OL website. Choose from "onlineliga.de," "onlineliga.ch," "
  onlineliga.at," or "onlineleague.co.uk."
- `-f, --file`: The path to the file containing the market scan config. (Default: "configuration_files/scan_market.yml")
- `-o, --output`: The directory where the output files will be stored. (Default: "output")

### Running the Script

To run the script, execute it using Python in the root directory of the project:

```bash
python onlineliga_script_collection/scan_market.py -l <location> -f <file> -o <output>
```

Replace <location> with the desired online league location, <file> with the path to the market scan config file,
and <output> with the output directory.

### Example Usage

```bash
python onlineliga_script_collection/scan_market.py -l onlineliga.at -f config/market_scan_config.yml -o data
```

This command will scan the player transfer market on the "onlineliga.at" website based on the configuration defined in "
config/market_scan_config.yml" and save the filtered offers to the "data" directory.

### Configuration File

The market scan config file allows you to define custom filter options for player offers. An example
configuration file is store in `configuration_files/scan_market.dist.yml`.

For instant usage, you can copy the file to `configuration_files/scan_market.yml` and run the script with the default
configuration.

The file looks like this:

```yaml
---
---
# mapping of player attributes
attributes:
    overall: 0
    age: 24
    marketValue: 38
    height: 27
    weight: 35
    countryCode: 25
    talent: 39
    rightFoot: 18
    leftFoot: 17
    shooting: 20
    power: 19
    technique: 16
    pace: 7
    heading: 14
    tackling: 15
    constitution: 8
    condition: 9
    fitness: 28
    gameInsight: 21
    keeperPlaceholder1: 1
    keeperOffTheLine: 3
    keeperPlaceholder2: 2
    keeperFoot: 5
    keeperGameOpening: 4
    keeperSweeper: 6

base_payload:
    "attrFilter[24][]": [ 17, 19 ]
    "custom[position]": 0
    "custom[transferFee][]": [ 0, 100000 ]
    "sort[by]": "tableHeaderRemainingTime"
    "sort[sorting]": "asc"
    "freeAgentsOnly": "false"
    "offersEnded": "false"
    liquidityCheck: "false"
    currentView: "transferList"
    page: 0

position:
    ST: 8
    OM: 16
    DM: 32
    IV: 64
    AV: 128
    TW: 256

filter:
# example for age filter
#  age:
#    min: 16
#    max: 18
#  countryCode:
#    min: 34
#    max: 34

headers:
    user_agent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
```

Feel free to modify and expand the configuration file to customize your player offer scanning criteria.

`player_attributes` defines the mapping of player attributes to their corresponding attribute IDs. You can find the
attribute IDs by inspecting the player attributes on the market page in OL.
You can also rename the keys to your liking. This will be used to display the headers in the output file.

`base_payload` defines the base payload for the market scan request.
**_Important Note:_** This section should be filled with caution since this is the initial request payload that will
be sent to the OL server. The looser the filter criteria, the more player offers will be retrieved. This can lead to
a high number of requests and potentially high server load.

`position` defines the mapping of player positions to their corresponding position IDs. You can swap
the `custom[position]`
value in the `base_payload` section with the desired position ID to filter for specific player positions.

`filter` defines the filter criteria for the player attributes. You can define a minimum and maximum value for each
attribute. The script will only retrieve player offers that match the defined criteria. the keys **must** match the
keys defined in `player_attributes`.

`headers` defines the headers that will be sent with the request. You can use this to set a custom user agent.

## Workflow

The script retrieves player offers from the market based on the defined `base_payload` filters.
It applies custom attribute filters to narrow down the player offers and retrieves detailed offer information.
The filtered player offers are saved to an Excel file for further analysis.
