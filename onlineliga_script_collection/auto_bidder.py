#!/usr/bin/env python3
import asyncio
import aiohttp
import argparse

from bs4 import BeautifulSoup

import shared.http as ol_http

from argparse import Namespace
from datetime import datetime, timedelta
from shared.login import login
from shared.location import select_location
from shared.http import fetch


def parse_arguments() -> Namespace:
    # Generate a parser instance
    parser = argparse.ArgumentParser(description='Place a bid on the transfer list',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-l', '--location', type=str, help='DE/AT/CH/UK', required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])
    parser.add_argument('-o', '--offer_id', type=int, help='The offer ID for the bid', required=True)
    parser.add_argument('-d', '--duration', type=int, help='The bid duration', required=True)
    parser.add_argument('-tf', '--transfer_fee', type=int, help='The transfer fee for the bid', required=True)
    parser.add_argument('-s', '--salary', type=int, help='The salary for the bid', required=True)
    parser.add_argument('-t', '--time', type=int,
                        help='The amount of time in seconds when to place the bid before the offer ends. Default: 15',
                        required=False,
                        default=15)

    args = parser.parse_args()

    if not args.location:
        args.location = select_location()
    return args


def parse_player_name(soup: BeautifulSoup) -> str:
    player_name = "n/a"
    player_name_element = soup.select_one(".player-complete-name .hidden-xs")
    if player_name_element:
        player_name = player_name_element.text
    return player_name


def parse_offer_end_date(soup: BeautifulSoup) -> datetime:
    offer_end_date = "n/a"
    offer_end_date_element = soup.select_one(".transfer-auction-end-date")

    if offer_end_date_element:
        utc_timestamp = offer_end_date_element.get("data-utc")

        if utc_timestamp:
            offer_end_date = datetime.strptime(utc_timestamp, "%Y-%m-%dT%H:%M:%S%z")
            offer_end_date = offer_end_date.astimezone().replace(tzinfo=None)

    return offer_end_date


async def wait_until_target_time(target_time: datetime, update_interval_seconds=60):
    now = datetime.now()
    wait_time = target_time - now
    total_seconds = wait_time.total_seconds()

    while total_seconds > 0:
        if total_seconds <= 120:  # 2 minutes left
            print(f"Final wait for {total_seconds} seconds to fire the bid.")
            await asyncio.sleep(total_seconds)
            break

        minutes, seconds = divmod(int(total_seconds), 60)
        print(f"Waiting for {minutes} minutes and {seconds} seconds...")
        await asyncio.sleep(update_interval_seconds)
        now = datetime.now()
        total_seconds = (target_time - now).total_seconds()

    print(f"Wait time has ended.")


async def place_bid(args, location, session):
    url = f"https://www.{location}/transferlist/bid"
    query_params = {
        "offerId": args.offer_id,
        "duration": args.duration,
        "transferFee": args.transfer_fee,
        "salary": args.salary
    }

    headers = {
        "User-Agent": f"Mozilla/5.0 (X11; Linux x86_64) "
                      f"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,*/*;q=0.8",
        "Accept-Encoding": "gzip, deflate, br",
        "Accept-Language": "en-US,en;q=0.5",
    }

    response = await ol_http.fetch(session, url, params=query_params, headers=headers)
    print(response)


async def fetch_offer(offer_id: str, location: str, session: aiohttp.ClientSession) -> dict:
    url = f"https://www.{location}/transferlist/getplayerview"
    query_params = {
        "offerId": offer_id
    }

    headers = {
        "User-Agent": f"Mozilla/5.0 (X11; Linux x86_64) "
                      f"AppleWebKit/537.36 (KHTML, like Gecko) Chrome/116.0.0.0 Safari/537.36"
    }

    response = await fetch(session, url, params=query_params, headers=headers)
    soup = BeautifulSoup(response, "lxml")
    offer = {}
    offer_player_name = parse_player_name(soup)
    offer_end_date = parse_offer_end_date(soup)
    offer["player_name"] = offer_player_name
    offer["offer_end_date"] = offer_end_date

    return offer


def verify_offer_end_date(offer: dict) -> bool:
    # only accept a valid offer end date
    if offer["offer_end_date"] == "n/a":
        print("Invalid offer end date.")
        return False
    elif offer["offer_end_date"] < datetime.now():
        print("Offer has already ended.")
        return False
    else:
        return True


async def main():
    args = parse_arguments()
    location = args.location

    async with aiohttp.ClientSession() as session:
        await login(session, location)
        offer = await fetch_offer(args.offer_id, location, session)

        # calculate the target time to place the bid
        time_before_bid = timedelta(seconds=args.time)
        target_time = offer["offer_end_date"] - time_before_bid

        if verify_offer_end_date(offer):
            print(f"Offer ID: {args.offer_id}\n"
                  f"Duration: {args.duration}\n"
                  f"Transfer Fee: {args.transfer_fee}\n"
                  f"Salary: {args.salary}")

            print(f"Additional info:\n"
                  f"Player Name: {offer['player_name']}\n"
                  f"Offer End Date: {offer['offer_end_date']}")

            print(f"Target time to place the bid: {target_time}")
            await wait_until_target_time(target_time)
            await place_bid(args, location, session)


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("KeyboardInterrupt")
        exit(0)
