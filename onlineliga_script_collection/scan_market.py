#!/usr/bin/env python3
import xlsxwriter
import asyncio
import json
import math
import aiohttp
import argparse
import shared.http as ol_http

from shared.tm_parsing import parse_attributes, parse_offer_details, parse_player_name, parse_player_position, \
    parse_additional_offer_details
from bs4 import BeautifulSoup
from argparse import Namespace
from datetime import datetime
from shared.login import login
from shared.location import select_location
from shared.config_parser import load_market_scan_config, config
from shared.player_id_parser import parse_player_id
from shared.tm_player_offer import PlayerOffer


def parse_arguments() -> Namespace:
    # arguments
    # generate a parser instance
    parser = argparse.ArgumentParser(description='set the location',
                                     formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('-l ', '--location', type=str, help='DE/AT/CH/UK', required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])
    parser.add_argument('-f', '--file',
                        type=str,
                        help='path to the file containing the market scan config',
                        required=False,
                        default="configuration_files/scan_market.yml")
    parser.add_argument("-o", "--output",
                        type=str,
                        help="directory where the output files will be stored",
                        required=False,
                        default="output")

    args = parser.parse_args()

    if not args.location:
        args.location = select_location()
    return args


async def transform_to_xlsx(offers, location: str, output_dir: str):
    # Pick a filename based on the current date and time
    # Pick a filename based on the current date and time and the location
    current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    file_name = f"{output_dir}/offer_{location}_{current_time}.xlsx"
    # Create a new Excel workbook and add a worksheet
    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()

    # Write headers for player attributes and offer details
    headers = ["PlayerID", "Name", "Positions"] + list(offers[0].attributes.keys()) + list(
        offers[0].offer_details.keys())
    for col, header in enumerate(headers):
        worksheet.write(0, col, header)

    # Write data for each offer
    for row, offer in enumerate(offers, start=1):
        worksheet.write(row, 0, offer.player_id)
        worksheet.write(row, 1, offer.player_name)
        positions = ", ".join(offer.player_positions)
        worksheet.write(row, 2, positions)

        # Write player attributes
        for col, attribute_value in enumerate(offer.attributes.values(), start=3):
            worksheet.write(row, col, attribute_value)

        # Write offer details
        for col, offer_detail_value in enumerate(offer.offer_details.values(), start=3 + len(offer.attributes)):
            worksheet.write(row, col, offer_detail_value)

    # Close the workbook
    workbook.close()


async def get_total_transfers_async(session: aiohttp.ClientSession, query_params: dict, headers: dict,
                                    transfer_list_url: str):
    data = await ol_http.fetch(session, transfer_list_url, params=query_params, headers=headers)
    total_soup = BeautifulSoup(data, "lxml")
    elements = total_soup.find_all(attrs={"id": "maxNumberOfRows"})
    number = int(elements[0]["data-value"])
    return number


def determine_pages(offers: int):
    offers_per_page = 15
    pages = math.ceil(offers / offers_per_page)
    return pages


async def download_transfer_pages_async(session: aiohttp.ClientSession, params: dict, headers: dict,
                                        transfer_list_url: str):
    total_transfers = await get_total_transfers_async(session, params, headers, transfer_list_url)
    total_pages = determine_pages(total_transfers)

    # urls is a list of tuples (url, params)
    urls = []

    for page in range(total_pages):
        page_params = params.copy()
        page_params["page"] = f"{page}"
        urls.append((transfer_list_url, page_params))

    downloaded_pages = await ol_http.fetch_all(session,
                                               urls,
                                               config["lock_delay"],
                                               config["concurrent_requests"],
                                               headers)

    return downloaded_pages


async def download_offer_details_async(session: aiohttp.ClientSession, offers: list, location: str, headers: dict):
    offer_details_base_url = f"https://www.{location}/transferlist/getplayerview"
    offer_details_urls = []
    for offer in offers:
        offer_details_urls.append((offer_details_base_url, {"offerId": offer.offer_details["offerId"]}))

    offer_details_downloads = await ol_http.fetch_all(session,
                                                      offer_details_urls,
                                                      config["lock_delay"],
                                                      config["concurrent_requests"],
                                                      headers)

    return offer_details_downloads


def process_offer_element(element, config_attributes, location):
    player_id = parse_player_id(element)
    player_name = parse_player_name(element)
    player_positions = parse_player_position(element)
    attributes_elements = element.find_all(attrs={"data-player-attributes": True})
    attributes = json.loads(attributes_elements[0]["data-player-attributes"])
    parsed_attributes = parse_attributes(attributes, config_attributes)
    talent_status = attributes_elements[0]["data-talent-state"]
    parsed_attributes["talentStatus"] = talent_status

    # parse the offer details
    offer_details = parse_offer_details(element)
    # add the url to the offer details
    offer_details["url"] = f"https://www.{location}/transferlist/gettransferlistview?offerId={offer_details['offerId']}"
    offer = PlayerOffer(player_id, player_name, player_positions, parsed_attributes, offer_details)
    return offer


async def main():
    args = parse_arguments()
    location = args.location
    output_dir = args.output
    market_scan_file_path = args.file

    market_config = load_market_scan_config(market_scan_file_path)
    headers = {"User-Agent": market_config["headers"]["user_agent"]}
    config_attributes = market_config["attributes"]
    query_params = market_config["base_payload"]
    filters = market_config["filter"]

    transfer_list_url = f"https://www.{location}/transferlist/gettransferlist"

    offers = []

    async with aiohttp.ClientSession() as session:
        token = await login(session, location)
        print(f"token: {token}")

        print(f"downloading transfer pages...")
        downloads = await download_transfer_pages_async(session, query_params, headers, transfer_list_url)
        print(f"downloaded {len(downloads)} transfer pages")

        print(f"processing {len(downloads)} transfer pages (parsing and applying filters)")
        for download in downloads:
            soup = BeautifulSoup(download, "lxml")
            elements = soup.find_all(attrs={"data-offer-id": True})
            for element in elements:
                if "id" in element.attrs:
                    offer = process_offer_element(element, config_attributes, location)
                    if PlayerOffer.apply_filter(offer, filters):
                        offers.append(offer)

        print(f"found {len(offers)} offers")

        print(f"downloading offer details for {len(offers)} offers...")
        offer_downloads = await download_offer_details_async(session, offers, location, headers)
        print(f"downloaded {len(offer_downloads)} offer details")
        print(f"processing {len(offer_downloads)} offer details")
        for offer_download in offer_downloads:
            offer_soup = BeautifulSoup(offer_download, "lxml")
            elements = offer_soup.find_all(attrs={"class": "bid-form"})
            for element in elements:
                additional_offer_details = parse_additional_offer_details(element)
                # add the additional offer details to all offers
                for offer in offers:
                    if offer.offer_details["offerId"] == additional_offer_details["offerId"]:
                        offer.offer_details.update(additional_offer_details)
                        break

    print(f"saving offers to xlsx file")
    await transform_to_xlsx(offers, location, output_dir)


if __name__ == '__main__':
    try:
        asyncio.run(main())
    except KeyboardInterrupt:
        print("KeyboardInterrupt")
        exit(0)
