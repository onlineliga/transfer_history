import json


def load_credentials(credentials_file) -> dict:
    creds = None
    try:
        with open(credentials_file, "r") as file:
            creds = json.load(file)
    except Exception as err:
        print(f"An error occurred while reading the credentials file: {err}"
              f"\nCreating default credentials...")
        create_default_credentials(credentials_file)
        creds = load_credentials(credentials_file)
    finally:
        return creds


def create_default_credentials(credentials_file):
    # Take appropriate action if the credentials could not be loaded
    default_creds = {
        "credentials": [
            {
                "login": "user@mail.org",
                "password": "xyz",
                "location": "onlineleague.co.uk"
            }
        ]
    }

    with open("configuration_files/credentials.json", "w") as file:
        json.dump(default_creds, file, indent=2)
