#!/usr/bin/env python3
import json
import re
from datetime import datetime

import bs4

from . import converter
from .converter import strip_whitespaces, remove_non_numeric
from .team_id_parser import parse_team_id


def parse_league(element: bs4.element) -> dict:
    # 5. ONLINELIGA\nSüd
    league = {}

    if element is None:
        print(f"Error parsing league: element is None")
        return league

    try:
        league_text = element.text
        league_text_stripped = strip_whitespaces(league_text)
        league_level = remove_non_numeric(league_text_stripped[0])

        league_name = format_league_name(league_text_stripped[3:])

        league['level'] = int(league_level)
        league['name'] = league_name
    except Exception as e:
        print(f"Error parsing league: {e}")
    return league


def format_league_name(league_name: str) -> str:
    return league_name.replace("\n", " ").strip()


def parse_capacity(element: bs4.element) -> int:
    capacity = -1

    if element is None:
        print(f"Error parsing capacity: element is None")
        return capacity

    try:
        capacity_text_raw = element.text
        capacity_text_stripped = strip_whitespaces(capacity_text_raw)
        capacity_text = remove_non_numeric(capacity_text_stripped)
        capacity = int(capacity_text)
    except Exception as e:
        print(f"Error parsing capacity: {e}")
    return capacity


def parse_accept_button(element: bs4.element) -> str:
    # enabled:
    # <button type="button" class="ol-button ol-button-state ol-btn-color-8  "
    # style="width: 100%;" onclick="olFriendlyRequest.details(521488);">
    # disabled:
    # <button type="button" class="ol-button ol-button-state ol-btn-color-7 ol-btn-disabled  " style="width: 100%;">
    # premium disabled:
    # <button type="button" class="ol-button ol-button-standard premium-locked " style="cursor: pointer;">
    accept_button = "enabled"

    if element is None:
        print(f"Error parsing accept button: element is None")
        return "n/a"

    try:
        if "ol-btn-disabled" in element["class"]:
            accept_button = "disabled"
        elif "premium-locked" in element["class"]:
            accept_button = "premium disabled"
    except Exception as e:
        print(f"Error parsing accept button: {e}")

    return accept_button


def parse_friendly_id(element: bs4.element) -> str:
    # <span id="olFriendlyStartTime-521482">8:00 - 9:00</span>
    friendly_id = "n/a"

    if element is None:
        print(f"Error parsing friendly ID: element is None")
        return friendly_id

    try:
        friendly_id = element["id"].split("-")[1]
    except Exception as e:
        print(f"Error parsing friendly ID: {e}")
    return friendly_id


def parse_friendly_date(element: bs4.element) -> str:
    # <script>$(function(){
    # olFriendlyTime.localize('#olFriendlyStartTime-64225',
    # ":min - :max",
    # {min:'2023-10-02T08:00:00+02:00',max:'2023-10-02T09:00:00+02:00'})});</script>

    friendly_date = "n/a"

    if element is None:
        print(f"Error parsing friendly date: element is None")
        return friendly_date

    # Define a regular expression pattern to extract the min and max values
    pattern = r"min:'([^']+)',max:'([^']+)'"
    match = re.search(pattern, element.text)

    if match is None:
        print(f"Error parsing friendly date: match is None")
        return friendly_date

    try:
        min_time = match.group(1)
        min_time = datetime.fromisoformat(min_time).strftime("%H:%M")
        max_time = match.group(2)
        max_time = datetime.fromisoformat(max_time).strftime("%H:%M")

        friendly_date = f"{min_time} - {max_time}"
    except Exception as e:
        print(f"Error parsing friendly date: {e}")

    return friendly_date


def parse_friendly_count(element: bs4.element) -> int:
    # class: ol-2-table-headline
    # text e.g. :32 Friendlies
    friendly_count = -1

    if element is None:
        print(f"Error parsing friendly count: element is None")
        return friendly_count

    try:
        friendly_count_text = element.text
        friendly_count_text_stripped = strip_whitespaces(friendly_count_text)
        friendly_count = remove_non_numeric(friendly_count_text_stripped)
    except Exception as e:
        print(f"Error parsing friendly count: {e}")

    return friendly_count


def parse_subpage(response: str) -> str:
    # response is a JSON string
    # { success: true, content: "<html>...</html>" , subload: 1 }
    # we need to extract the content
    subpage = ""
    # we use JSON decode to extract the content

    try:
        json_response = json.loads(response)
        subpage = json_response["content"]
    except json.JSONDecodeError as je:
        print(f"Error parsing subpage: {je}")
    return subpage


def parse_accept_response(response: str) -> dict:
    # {
    #   "success":true,
    #   "type":"msg",
    #   "header":"FRIENDLY ANNEHMEN",
    #   "yes":"Okay",
    #   "no":"Abbrechen",
    #   "content":"Du hast erfolgreich ein Angebot f\u00fcr die 35. Woche angenommen."
    # }
    try:
        json_response = json.loads(response)
        return json_response
    except json.JSONDecodeError as je:
        print(f"Error parsing friendly accept response: {je}")
        return {}


def parse_friendly_element(friendly_element: bs4.element) -> dict:
    friendly_detail_elements = friendly_element.select(".ol-friendly-requests-table-col")
    if not friendly_detail_elements:
        print(f"Error parsing friendly request: {friendly_element}")
        return {}

    if len(friendly_detail_elements) != 5:
        print(f"Error parsing friendly request: {friendly_detail_elements}")
        return {}

    team_name = friendly_detail_elements[0].text
    team_id = parse_team_id(friendly_detail_elements[0].select_one(".ol-team-name"))
    league = parse_league(friendly_detail_elements[1])
    capacity = parse_capacity(friendly_detail_elements[2])
    friendly_id = parse_friendly_id(friendly_detail_elements[3].find(id=re.compile(r"olFriendlyStartTime-(\d+)")))
    time_element = friendly_detail_elements[3].select_one("script")
    date = parse_friendly_date(time_element)

    accept_button = parse_accept_button(friendly_detail_elements[4].select_one("button"))

    friendly = {
        "team_name": converter.strip_whitespaces(team_name),
        "team_id": team_id,
        "league_level": league["level"],
        "league_name": league["name"],
        "capacity": capacity,
        "friendly_id": friendly_id,
        "date": date,
        "accept_button": accept_button
    }
    return friendly
