#!/usr/bin/env python3

import bs4


def parse_team_id(element: bs4.element) -> str:
    """
    Extracts the team ID from an HTML element.

    Args:
        element (bs4.element): A BeautifulSoup element containing information about a team.

    Returns:
        str: The team ID if found, or "n/a" if not found.

    Raises:
        Exception: If there is an error while parsing the team ID.

    Example:
        element = BeautifulSoup('<div onclick="userId : 123 });">Team A</div>', 'html.parser')
        parse_team_id(element)
        '123'
    """
    team_id = "n/a"
    try:
        team_id_target_text = element["onclick"]

        if "userId" in team_id_target_text:
            splits = team_id_target_text.split("userId : ", 1)

            if len(splits) > 1:
                team_id = splits[1].split(" });", 1)[0]
                return team_id

        return team_id
    except Exception as e:
        print(f"Error parsing team ID: {e}")
        return team_id
