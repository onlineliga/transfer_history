#!/usr/bin/env python3
def remove_non_numeric(string: str) -> str:
    return ''.join(i for i in string.strip() if i.isdigit())


def strip_whitespaces(string: str) -> str:
    return string.strip()
