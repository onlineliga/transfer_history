#!/usr/bin/env python3
import aiohttp
import asyncio
import tqdm


async def fetch(session: aiohttp.ClientSession, url: str, params=None, headers=None) -> str:
    async with session.get(url, params=params, headers=headers) as resp:
        page = await resp.text()
        return page


async def bound_fetch(sem: asyncio.Semaphore, session: aiohttp.ClientSession, url: str, delay: int, params=None,
                      headers=None, fetch_func: callable = fetch):
    async with sem:
        # When workers hit the limit, they'll wait for the specified delay
        # before making more requests.
        if sem.locked():
            await asyncio.sleep(delay)
        return await fetch_func(session, url, params, headers)


async def fetch_all(session: aiohttp.ClientSession, urls: list, delay: int, concurrent_requests: int,
                    headers=None, fetch_func: callable = fetch):
    semaphore = asyncio.Semaphore(concurrent_requests)

    tasks = []
    # urls is a list of tuples (url, params)
    for url in urls:
        tasks.append(asyncio.ensure_future(bound_fetch(semaphore, session, url[0], delay, url[1], headers, fetch_func)))

    responses = []
    for task in tqdm.tqdm(asyncio.as_completed(tasks), total=len(tasks)):
        responses.append(await task)

    return responses
