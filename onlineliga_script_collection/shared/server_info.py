#!/usr/bin/env python3

from .http import fetch
import aiohttp
import json


async def fetch_info(session: aiohttp.ClientSession, location: str) -> dict:
    """
    Fetches information from a specified onlineliga location using an asynchronous HTTP session.

    Args:
        session (aiohttp.ClientSession): An aiohttp ClientSession for making asynchronous HTTP requests.
        location (str): The location URL from which to fetch information.

    Returns:
        dict: A dictionary containing the fetched information, or an empty dictionary if an error occurs.

    Raises:
        aiohttp.ClientError: If there is an error with the aiohttp client while making the request.
        json.JSONDecodeError: If there is an error parsing the JSON response.
    """
    info = {}
    url = f"https://www.{location}/system/info"

    try:
        response = await fetch(session, url)
        info = json.loads(response)
    except aiohttp.ClientError as ce:
        print(f"AIOHTTP error while fetching info: {ce}")
    except json.JSONDecodeError as je:
        print(f"Error parsing server info: {je}")

    return info
