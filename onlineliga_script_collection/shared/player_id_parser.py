import re

import bs4


def parse_training_player_id(element: bs4.element) -> int:
    pid_regex = re.compile("playerId")
    player_id_elements = element.find_all(onclick=pid_regex)
    player_id_element = player_id_elements[0]
    player_id_target_text = player_id_element["onclick"]
    splits = player_id_target_text.split("'playerId': ", 1)
    player_id = splits[1].split(" }", 1)[0]
    return player_id


def parse_player_id(element: bs4.element) -> int:
    pid_regex = re.compile("playerId")
    player_id_elements = element.find_all(onclick=pid_regex)
    player_id_element = player_id_elements[0]
    player_id_target_text = player_id_element["onclick"]
    splits = player_id_target_text.split("playerId: ", 1)
    player_id = splits[1].split(" }", 1)[0]
    return player_id
