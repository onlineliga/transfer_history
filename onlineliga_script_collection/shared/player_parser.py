#!/usr/bin/env python3
from bs4 import BeautifulSoup
from .converter import remove_non_numeric

Name = 0
Country = 1
TeamName = 2
Age = 3
Position = 4
Weight = 5
Height = 6
BirthWeek = 7
MarketValue = 8
Foot = 9
Salary = 10
Contract = 11
Loyalty = 12
Bookings = 13


class Player:
    def __init__(self):
        self.ID = 0
        self.TeamID = 0
        self.Name = ""
        self.Country = ""
        self.Age = 0.0
        self.BirthSeason = 0.0
        self.BirthWeek = 0.0
        self.Salary = 0.0
        self.MarketValue = 0.0
        self.Weight = 0.0
        self.Height = 0.0
        self.Talent = 0.0
        self.Fitness = 0.0
        self.Overall = 0.0
        self.TalentDiscovered = False
        self.ContractExpiring = False
        self.Positions = []
        self.Abilities = {}

    def get_profile(self, profile_doc):
        soup = BeautifulSoup(profile_doc, "lxml")
        profile = player_array(soup, '.ol-player-table-row')
        self.Name = profile[Name]
        self.Country = profile[Country]
        self.Age = remove_non_numeric(profile[Age])
        self.Weight = remove_non_numeric(profile[Weight])
        self.Height = remove_non_numeric(profile[Height])
        self.BirthWeek = remove_non_numeric(profile[BirthWeek])
        self.MarketValue = remove_non_numeric(profile[MarketValue])
        self.Salary = remove_non_numeric(profile[Salary])
        self.Positions = profile[Position]

    def get_abilities(self, abilities_doc):
        soup = BeautifulSoup(abilities_doc, "lxml")
        abilities = player_map(soup, '.ol-playerabilitys-table-row')
        self.Abilities = abilities

    def get_talent_status(self):
        if self.Talent > 0:
            self.TalentDiscovered = True
        else:
            self.TalentDiscovered = False


def player_array(soup: BeautifulSoup, find_query: str):
    selection = soup.select(find_query)
    values = []

    for s in selection:
        try:
            s = s.findChildren()[-1]
            text = s.text
            text = text.strip()
        except Exception as e:
            print(f"An error occurred while parsing player: {e}")
            text = "n/a"
        values.append(text)

    return values


def player_map(soup: BeautifulSoup, find_query: str):
    selection = soup.select(find_query)
    values = {}

    for s in selection:
        first_child = s.findChildren()[0]
        key = first_child.get_text(strip=True)

        if key == "Talent":
            # find talent_discovered element
            talent_discovered_element = s.select_one('.ol-value-bar-small-label-special-type')
            # check whether element exists and whether it contains text
            if talent_discovered_element and talent_discovered_element.get_text(strip=True):
                values["TalentDiscovered"] = 0
            else:
                values["TalentDiscovered"] = 1

        # find value element
        value_element = s.select_one('.ol-value-bar-small-label-value')
        if value_element:
            value = value_element.get_text(strip=True)
        else:
            value = "n/a"

        values[key] = value
    return values
