import json
import yaml


def load_config(config_file):
    try:
        global config
        with open(config_file, "r") as file:
            config = json.load(file)
            return config
    except Exception as err:
        raise Exception(f"An error occurred while reading the config file: {err}")


def create_default_config():
    # Take appropriate action if the config could not be loaded
    default_config = {
        "concurrent_requests": 5,
        "lock_delay": 1,
        "credentials": {

        }
    }

    with open("configuration_files/config.json", "w") as file:
        json.dump(default_config, file, indent=2)


def load_market_scan_config(config_file):
    try:
        with open(config_file, "r") as file:
            market_scan_config = yaml.safe_load(file)
            return market_scan_config
    except Exception as exc:
        print(f"An error occurred {exc}")


try:
    config = load_config("./configuration_files/config.json")
except Exception as e:
    print(f"Error while loading the config file: {e}")
    print(f"Creating default configuration...")
    create_default_config()
    config = load_config("./configuration_files/config.json")
