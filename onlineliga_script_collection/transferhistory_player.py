#!/usr/bin/env python3
import argparse
from argparse import Namespace
from datetime import datetime

import aiohttp
import asyncio
import time

import bs4
import xlsxwriter
from shared.player_parser import Player
import shared.http as ol_http
from bs4 import BeautifulSoup
from shared.location import select_location
import sys
import tqdm

from shared.config_parser import config

start_time = time.time()


def parse_arguments() -> Namespace:
    # arguments
    # generate a parser instance
    parser = argparse.ArgumentParser(description='scrape transfer history of a player in onlineliga',
                                     formatter_class=argparse.RawTextHelpFormatter)

    parser.add_argument('start', type=int,
                        help='The start player ID.')

    parser.add_argument('end', type=int,
                        help="The end player ID.")

    parser.add_argument('-l ', '--location', type=str, help='DE/AT/CH/UK', required=False,
                        choices=["onlineliga.de", "onlineliga.ch", "onlineliga.at", "onlineleague.co.uk"])

    parser.add_argument("-o", "--output",
                        type=str,
                        help="directory where the output files will be stored",
                        required=False,
                        default="output")

    args = parser.parse_args()
    if not args.location:
        args.location = select_location()
    return args


def parse_team_id(element: bs4.element):
    team_element = element.find(attrs={"class": "ol-team-name"})
    if not team_element:
        team_element = element.find(attrs={"class": "ol-team-name-inactive"})
    if team_element:
        team_id_attr = team_element["onclick"]
        player_id = team_id_attr.split("userId : ", 1)[1].split(" }", 1)[0]
        return player_id
    return ""


def parse_season(element: bs4.element):
    season_group_element = element.find(attrs={"class": "transfer-history-inner-row"})
    season = season_group_element.find('div').text
    return season


def parse_match_day(element: bs4.element):
    match_day_group_element = element.find(attrs={"class": "transfer-history-inner-row"})
    match_day = match_day_group_element.find_all('div')[1].text
    return match_day


def parse_transfer_history(team, player_id: int) -> list:
    transfer_history = []
    for element in team:
        if is_youth_player(element):
            player_id = player_id
            season = parse_season(element)
            match_day = parse_match_day(element)
            team_id = parse_team_id(element)
            transfer_history = [player_id, season, match_day, team_id]
    return transfer_history


def is_youth_player(element: bs4.element):
    former_team_elements = element.find_all(attrs={"class": "transferhistory-former-team"})
    if former_team_elements:
        for former_team_element in former_team_elements:
            team_text = former_team_element.text
            if "Jugend" in team_text or "Youth" in team_text:
                return True
    return False


def write_xlsx(transfer_list, location: str, output_dir: str):
    # Pick a filename based on the current date and time and the location
    current_time = datetime.now().strftime("%Y-%m-%d_%H-%M-%S")
    # file_name = f"youth_{location}_{current_time}.xlsx"
    file_name = f"{output_dir}/youth_{location}_{current_time}.xlsx"

    workbook = xlsxwriter.Workbook(file_name)
    worksheet = workbook.add_worksheet()

    headers = ["PlayerID", 'Season', 'Matchday', "TeamID", "PlayerName", "Positions", "Age", "BirthWeek", "Country",
               "MarketValue", "Overall", "Talent", "TalentDiscovered"]

    for i, header in enumerate(headers):
        worksheet.write(0, i, header)

    for i, transfer in enumerate(transfer_list):
        if transfer:
            for j, detail in enumerate(transfer):
                worksheet.write(i + 1, j, detail)

    workbook.close()


def process_transfer_history(transfer_history_page, player_id: int) -> list:
    soup = BeautifulSoup(transfer_history_page, "lxml")
    transfer_elements = soup.find_all(attrs={"class": "player-marketvalue-table"})
    transfer_history = parse_transfer_history(transfer_elements, player_id)
    return transfer_history


def process_player_details(overview_page) -> list:
    player = Player()
    player.get_profile(overview_page)
    player_details = [player.Name, player.Positions, player.Age, player.BirthWeek, player.Country, player.MarketValue]

    player.get_abilities(overview_page)
    # search dictionary for key "FÄHIGKEITEN" or "SKILLS" and return the value if found
    skill_key = next((key for key in player.Abilities.keys() if key in ["FÄHIGKEITEN", "SKILLS"]), None)
    if skill_key:
        overall = player.Abilities[skill_key]
        player_details.append(overall)

    # same for "Talent"
    talent_key = next((key for key in player.Abilities.keys() if key in ["Talent"]), None)
    if talent_key:
        talent = player.Abilities[talent_key]
        player_details.append(talent)

    talent_discovered_key = next((key for key in player.Abilities.keys() if key in ["TalentDiscovered"]), None)
    if talent_discovered_key:
        talent_discovered = player.Abilities[talent_discovered_key]
        player_details.append(talent_discovered)

    return player_details


async def fetch_transfer_history(session: aiohttp.ClientSession, url: str, params=None,
                                 headers=None):
    async with session.get(url, params=params, headers=headers) as resp:
        page = await resp.text()
        transfer_history = process_transfer_history(page, params["playerId"])
        return transfer_history


async def fetch_player_details(session: aiohttp.ClientSession, url: str, params=None,
                               headers=None):
    async with session.get(url, params=params, headers=headers) as resp:
        page = await resp.text()
        player_overview = process_player_details(page)
        return player_overview


async def fetch_all(player_id_start: int, player_id_end: int, location: str, fetch_func, output_dir: str):
    async with aiohttp.ClientSession() as session:
        semaphore = asyncio.Semaphore(config["concurrent_requests"])

        tasks = []
        for player_id in range(player_id_start, player_id_end):
            query_params = {"playerId": player_id}
            transfer_history_url = f'https://www.{location}/player/transferhistory'
            details_url = f'https://www.{location}/player/details'

            tasks.append(asyncio.ensure_future(ol_http.bound_fetch(semaphore, session, transfer_history_url,
                                                                   config["lock_delay"], query_params,
                                                                   fetch_func=fetch_func)))
        responses = []
        for f in tqdm.tqdm(asyncio.as_completed(tasks), total=len(tasks)):
            response = await f
            if response:
                details = await ol_http.bound_fetch(semaphore, session, details_url, config["lock_delay"],
                                                    {"playerId": response[0]}, fetch_func=fetch_player_details)
                # combine the transfer history with the player details
                response.extend(details)
                responses.append(response)

        write_xlsx(responses, location, output_dir)


def main():
    args = parse_arguments()
    if sys.version_info[0] == 3 and sys.version_info[1] >= 8 and sys.platform.startswith('win'):
        asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())
    asyncio.run(fetch_all(args.start, args.end + 1, args.location, fetch_transfer_history, args.output))
    print(f"--- %s seconds ---" % (time.time() - start_time))


if __name__ == '__main__':
    main()
