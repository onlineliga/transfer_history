#!/bin/sh

# Define the directory containing the Python scripts
SCRIPTS_DIR="/ol/scripts"

# Check if a command was provided
if [ $# -eq 0 ]; then
    echo "Usage: entrypoint.sh <script_name> [args...]"
    exit 1
fi

# Extract the script name (first argument)
script_name="$1"
shift  # Remove the first argument (script name)

# Check if the script file exists in the scripts directory
script_path="${SCRIPTS_DIR}/${script_name}.py"
if [ ! -f "$script_path" ]; then
    echo "Script '${script_name}' not found in ${SCRIPTS_DIR}."
    exit 1
fi

# Run the script with the provided arguments
python "${script_path}" "${@}"
